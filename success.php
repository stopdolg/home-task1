<?php
require_once 'include.php';
?>

<link rel="stylesheet" href="style.css" />

<div class='myForm'>
    <div class='myForm-contain'>
        <h3>Hello, <?=getUserName(); ?>. Вы зашли <?=getUserInputs(); ?> раз.</h3>

        <div>
            <a href="form-handler.php?logout=yes" class="button">Logout</a>
        </div>
    </div>
</div>
