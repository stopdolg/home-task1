<?php
require_once 'include.php';
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <div class='myForm'>
            <form method='post' action='form-handler.php' name='myForm'>
                <div class='myForm-contain'>
                    <p>
                        <span class='title'><label for='input-login'>Login</label></span><br />
                        <input type='text' name='login' size='20' placeholder='user@mail.com' id='input-login' />
                    </p>
                    <p>
                        <span class='title'><label for='input-psw'>Password</label></span><br />
                        <input type='password' name='password' size='20' placeholder='Password' id='input-psw' />
                    </p>
                    <div>
                        <button type='submit' name='myForm-btn' class='button'>Войти</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>