<?php
session_start();
define("FILE_LOG_LOGIN", "login.log");

function saveLogin($userLogin)
{
    if(isset($userLogin)){
        date_default_timezone_set('ASIA/Novosibirsk');

        $text = $userLogin.' '.date('d.m.Y H:i:s').PHP_EOL;
        file_put_contents(FILE_LOG_LOGIN, $text, FILE_APPEND | LOCK_EX);
    }

}

function countLogining($userLogin) {
    
    $data = file_get_contents(FILE_LOG_LOGIN);
    $arVal = preg_split("/[\s-\n]/", $data);
    $arValCount = array_count_values($arVal);
    return $arValCount[$userLogin];
    
//    foreach ($data as $value) {
//        $arVal = explode(" ", $value);
//        if($arVal[0] == $userLogin) {
//            
//        }
//    }
}

function login($login, $password)
{
    if(!empty($login) && !empty($password)) {
        saveLogin($login);
        $number = countLogining($login);
        
        $_SESSION['user'] = [
            'name' => $login,
            'inputs' => $number
        ];
             
        return true;
    } else {
        return false;
    }
}

function logout()
{
    session_destroy();
    header('Location: index.php');
    
    exit();
}

function getUserName()
{
    return $_SESSION['user']['name'] ?? 'unknown';
}

function getUserInputs()
{
    return $_SESSION['user']['inputs'] ?? '1';
}

function redirectOnLogin($loginResult)
{
    if (!empty($loginResult)) {
        header("Location: success.php");
    } else {
        header("Location: failed.php");
    }
    exit();
}