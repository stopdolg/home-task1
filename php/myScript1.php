<?php

//task 1

$arFruit = ['apple', 'banana', 'grape', 'orange', 'kiwi'];

echo '! for <br>';
for($i = 0; $i <  count($arFruit); $i++) {
    echo $arFruit[$i].'<br>';
}

echo '<br>! foreach <br>';
foreach($arFruit as $value) {
    echo $value.'<br />';
}

echo '<br>! while <br>';
$cnt = 0;
while($cnt < count($arFruit)) {
    echo $arFruit[$cnt].'<br>';
    $cnt++; 
}

//task 2

echo '<br>! array_map <br>';
$echoFunc = function($el) {
    echo $el.'<br>';
};
$b = array_map($echoFunc, $arFruit);

echo '<br>! array_walk <br>';
array_walk($arFruit, function(&$num) {
    echo $num.'<br>';
});

//task 3 - перебрать фрукты, которые не банан
echo '<br>! without banana <br>';
foreach($arFruit as $value) {
    if($value == 'banana') {
        continue;
    }
    echo $value.'<br />';
}

echo '<br>! without banana - array_filter <br>';
function doFilter ($var) {
    return !($var == 'banana');
}

print_r(array_filter($arFruit, 'doFilter'));
