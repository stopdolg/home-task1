<?php

//$fruits = ['banana', 'apple', 'orange'];
//
//function getFruit(array $arFruits, $needleFruit) {
//    foreach($arFruits as $fruit) {
//        if($fruit == $needleFruit) {
//            return $fruit;
//        }
//    }
//}
//
//getFruit($fruits, 'banana');

$fileName = 'test-file';

function createFile($fileName) 
{
    $fileRes = fopen($fileName, 'w');
    fwrite($fileRes, 'Hello' . PHP_EOL);
    fclose($fileRes);
}

function readMyFile($fileName)
{
    $fileRes = fopen($fileName, 'r');
    
    while ($line = fgets($fileRes)) {
        echo $line;
    }
    fclose($fileRes);
}

createFile($fileName);

readMyFile($fileName);