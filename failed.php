<?php
require_once 'include.php';
?>

<link rel="stylesheet" href="style.css" />

<div class='myForm'>
    <div class='myForm-contain'>
        <h3>Incorrect username or password.</h3>

        <div>
            <a href="index.php" class="button">Try again</a>
        </div>
    </div>
</div>
